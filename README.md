# Python html scraper

## Installing
`pip install -r requirements.txt`

## Simple usage
`python3 main.py -i input.zip`

## Help
`python3 main.py -h`
