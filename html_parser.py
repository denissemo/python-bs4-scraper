import os
import logging
from urllib.parse import urlparse, urljoin

import requests
from bs4 import BeautifulSoup


class HtmlLinkParser:
    def __init__(self, filepath, filename):
        self.path = os.path.join(filepath, filename)

        if not os.path.isfile(self.path):
            raise IsADirectoryError(f'"{self.path}" is not file')

        self.bad_links = []

        with open(self.path) as html_file:
            self._soup = BeautifulSoup(html_file, 'html.parser')

    def start(self):
        logging.info(f'PROCESSING: {self.path}')
        links = self._soup.find_all('a')

        for index, link in enumerate(links):
            href = link.get('href')

            if href is not None:
                try:
                    url = urlparse(href)
                    res = requests.get(
                        urljoin('http://127.0.0.1:8080/',
                                href) if url.scheme == '' else href)
                    logging.info(f'GET {href} {res.status_code}')

                    if res.status_code == 404:
                        self._remove_href_and_change_tag(link, href)
                except Exception:
                    self._remove_href_and_change_tag(link, href)
            else:
                self._remove_href_and_change_tag(link)
            logging.info(f'Done {index + 1} of {len(links)}')

        self._rewrite_html_file()
        logging.info(f'FINISHED: {self.path}')

    def _remove_href_and_change_tag(self, link_obj, href = None, tag = 'span'):
        logging.warning(f'BAD LINK: {href}')
        if href:
            del link_obj['href']
        link_obj.name = tag

        self.bad_links.append(href)

    def _rewrite_html_file(self):
        if self.bad_links:
            logging.info(f'REWRITE: {self.path}')
            os.remove(self.path)
            with open(self.path, 'w') as new_file:
                new_file.write(self._soup.prettify())
