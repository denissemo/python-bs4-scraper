import os
import sys
import logging
import argparse

from worker import ZipWorker

if __name__ == '__main__':
    logging.basicConfig(format='%(levelname)s [%(asctime)s]: %(message)s',
                        level=logging.INFO)

    arg_parser = argparse.ArgumentParser()

    required = arg_parser.add_argument_group('required arguments')
    required.add_argument('--input', '-i', help='Input zip filename',
                          type=str)

    arg_parser.add_argument('--out', '-o', help='Output zip filename',
                            default='result', type=str)
    arg_parser.add_argument('--max-workers', '-w', help='Max thread workers',
                            default=4, type=int)
    args = arg_parser.parse_args()

    if not args.input:
        arg_parser.print_help()
        sys.exit(0)
    if not os.path.isfile(args.input):
        raise IsADirectoryError(f'"{args.input}" is not file')

    input_zip_filename = args.input
    output_zip_filename = args.out

    worker = ZipWorker(input_zip_filename, output_zip_filename,
                       args.max_workers)
    worker.start()
