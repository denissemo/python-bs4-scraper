import threading
import logging
import http.server
import socketserver


class Handler(http.server.SimpleHTTPRequestHandler):
    STATIC_PATH = 'static'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs, directory=self.STATIC_PATH)


class StaticServer:
    PORT = 8080

    def __init__(self, static_path = 'static'):
        Handler.STATIC_PATH = static_path

        self._server = socketserver.TCPServer(('localhost', self.PORT), Handler)
        self._thread = threading.Thread(target=self._server.serve_forever)
        self._thread.daemon = True

    def start(self):
        logging.info(f'Server started at localhost:{self.PORT}')
        self._thread.start()

    def stop(self):
        self._server.shutdown()
        logging.info(f'Server stopped')
