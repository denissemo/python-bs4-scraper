import os
import csv
import shutil
import zipfile
import concurrent.futures

from html_parser import HtmlLinkParser
from server import StaticServer


class ZipWorker:
    def __init__(self, input_file, out_file, max_workers = 4):
        self._out_file = out_file
        self._tmp_folder_name = os.path.splitext(input_file)[0]
        self._static_path = 'static'
        self._max_workers = max_workers

        with zipfile.ZipFile(input_file) as zip_file:
            zip_file.extractall(self._tmp_folder_name)
            # For local static serving
            zip_file.extractall(self._static_path)

        self._folder_iterator = os.walk(self._tmp_folder_name)
        self._server = StaticServer()
        self._server.start()

    def start(self):
        with concurrent.futures.ThreadPoolExecutor(
                max_workers=self._max_workers
        ) as executor:
            executor.map(self._process, self._folder_iterator)

        shutil.make_archive(self._out_file, 'zip', self._tmp_folder_name)

        shutil.rmtree(self._tmp_folder_name)
        shutil.rmtree(self._static_path)

        self._server.stop()

    @staticmethod
    def _process(filepath):
        path, folders, files = filepath
        files = list(filter(lambda x: x.endswith('.html'), files))

        csv_writer = CsvWriter()

        for file in files:
            parser = HtmlLinkParser(path, file)
            parser.start()
            csv_writer.append_data(parser.path, parser.bad_links)

        csv_writer.write()


class CsvWriter:
    def __init__(self):
        self._data = []

    def append_data(self, path, links):
        for link in links:
            self._data.append([path, link])

    def write(self):
        if not os.path.isfile('bad_links.csv'):
            self._data.insert(0, ['filepath', 'link'])

        with open(f'bad_links.csv', 'a') as csv_file:
            writer = csv.writer(csv_file)
            writer.writerows(self._data)
